echo
cp "$1" tempush.cc

	python << END 

		f = open('tempush.cc', 'r')
		ush = f.read()
		f.close()

		ush = ush.replace('ush', '').replace('Ush', '').replace('USH', '')

		f = open('tempush.cc', 'w')
		f.write(ush)
		f.close()

	END

g++ -Wall tempush.cc -o ushush

chmod +x ushush

./ushush > ushush.txt

	python << END 

		f = open('ushush.txt', 'r')
		ush = f.read()
		f.close()
		print (ush.replace(' ', 'ush '))

	END

rm tempush* ushush*

echo 
echo
